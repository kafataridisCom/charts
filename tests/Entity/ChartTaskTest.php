<?php

namespace App\Tests\Entity;

use PHPUnit\Framework\TestCase;
use App\Entity\ChartTask;

class ChartTaskTest extends TestCase {

    public function testEntityGetterSetter() {
        $chartTask = new ChartTask();

        $chartTask->setEmail('test@example.com');
        $chartTask->setCompanySymbol('APPL');
        $chartTask->setEndDate('2020-10-01');
        $chartTask->setStartDate('2020-10-01');

        $this->assertEquals('test@example.com', $chartTask->getEmail());
        $this->assertEquals('APPL', $chartTask->getCompanySymbol());
        $this->assertEquals('2020-10-01', $chartTask->getEndDate());
        $this->assertEquals('2020-10-01', $chartTask->getStartDate());
    }

    public function testEntityGetterSetterNotEquals() {
        $chartTask = new ChartTask();

        $chartTask->setEmail('test@example.com');
        $chartTask->setCompanySymbol('APPL');
        $chartTask->setEndDate('2020-10-01');
        $chartTask->setStartDate('2020-10-01');

        $this->assertNotEquals('stest@example.com', $chartTask->getEmail());
        $this->assertNotEquals('wAPPL', $chartTask->getCompanySymbol());
        $this->assertNotEquals('2020-10-02', $chartTask->getEndDate());
        $this->assertNotEquals('2020-10-03', $chartTask->getStartDate());
    }

}
