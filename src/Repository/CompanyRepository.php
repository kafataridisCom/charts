<?php

namespace App\Repository;

use App\Entity\Company;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Company|null find($id, $lockMode = null, $lockVersion = null)
 * @method Company|null findOneBy(array $criteria, array $orderBy = null)
 * @method Company[]    findAll()
 * @method Company[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanyRepository extends ServiceEntityRepository {
    /**
     * CompanyRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Company::class);
    }

    /**
     * @param String $name
     * @param String $symbol
     * @return mixed[]
     * @throws \Doctrine\DBAL\DBALException
     */
    public function searchEntitiesByString(String $name, String $symbol) {

        $conn = $this->getEntityManager()->getConnection();

        $sql = "
            SELECT 
                c.symbol, c.name 
            FROM 
                company c
            WHERE
                (c.symbol LIKE :symbol OR c.name LIKE :name)
            ORDER BY c.name ASC
        ";
        $stmt = $conn->prepare($sql);
        $stmt->execute(['symbol'=>'%'.$symbol.'%','name'=>'%'.$name.'%']);

        // returns an array of arrays (i.e. a raw data set)
        return $stmt->fetchAll();
    }

    /**
     * @param String $symbol
     * @return array|mixed
     * @throws \Doctrine\DBAL\DBALException
     */
    public function findCompanyBySymbol(String $symbol) {

        $conn = $this->getEntityManager()->getConnection();

        $sql = "
            SELECT 
                c.id, c.name
            FROM 
                company c
            WHERE 
                c.symbol = :symbol 
        ";
        $stmt = $conn->prepare($sql);
        $stmt->execute(['symbol'=>$symbol]);
        $results = $stmt->fetchAll();
        if (isset($results[0])) {
            return $results[0];
        }

        return [];
    }
}