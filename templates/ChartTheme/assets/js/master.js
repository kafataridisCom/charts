skipValidation = false;
companySymbol = '';
chartColors = {
    red: 'rgb(255, 99, 132)',
    orange: 'rgb(255, 159, 64)',
    yellow: 'rgb(255, 205, 86)',
    green: 'rgb(75, 192, 192)',
    blue: 'rgb(54, 162, 235)',
    purple: 'rgb(153, 102, 255)',
    grey: 'rgb(201, 203, 207)'
};
$(function() {
    function formValidation() {
        var chartForm = $('form[name="chart_form"]');
        chartForm.on('submit', function (e) {
            if (window.skipValidation == true) {
                return;
            }
            var formData = $(this).serialize();
            $.ajax({
                url: '/async/checkform',
                type: 'POST',
                data: formData,
                dataType: "JSON",
                success: function (data) {
                    if (data.success) {
                        window.skipValidation = true;
                        chartForm.submit();
                    } else {
                        $('input[name^="chart_form[').removeClass('is-invalid');
                        $.each(data.errors, function (e, v) {
                            var inputElem = $('input[name="chart_form[' + e + ']"]');
                            if (inputElem.length) {
                                var html = '<ul>';
                                $.each(v, function (i, error) {
                                    html += '<li>' + error + '</li>'
                                });
                                inputElem.parent().find('.invalid-feedback').html(html);
                                inputElem.addClass('is-invalid');
                            }
                        });
                        return false;
                    }

                }
            });
            e.preventDefault();
        });
    }

    // Datepickers
    // Do not allow date end datepicker to set older date than start datepicker
    // allow same day
    var $startDate = $('#chart_form_startDate'),
        $endDate = $('#chart_form_endDate');
    $startDate.datepicker({
        dateFormat: "yy-mm-dd",
        onSelect: function (date) {
            var date2 = $startDate.datepicker('getDate');
            date2.setDate(date2.getDate()); // to enable one diff add +1 in the setDate
            $endDate.datepicker('setDate', date2);
            //sets minDate to dt1 date + 1
            $endDate.datepicker('option', 'minDate', date2);
        }
    });
    $endDate.datepicker({
        dateFormat: "yy-mm-dd",
        onClose: function () {
            var dt1 = $startDate.datepicker('getDate'),
                dt2 = $endDate.datepicker('getDate');
            //check to prevent a user from entering a date below date of dt1
            if (dt2 < dt1) {
                var minDate = $('#dt2').datepicker('option', 'minDate');
                $('#dt2').datepicker('setDate', minDate);
            }
        }
    });

    // Auto complete
    var $companySymbol = $('#chart_form_companySymbol'),
        options = {
            url: function(phrase) {
                return "/api/company/search";
            },
            getValue: function(element) {
                return element.symbol;
            },
            template: {
                type: "description",
                fields: {
                    description: "name"
                }
            },
            list: {
                onClickEvent: function() {
                    console.log($companySymbol.val());
                }
            },
            ajaxSettings: {
                dataType: "json",
                method: "POST",
                data: {
                    dataType: "json"
                }
            },
            preparePostData: function(data) {
                data.phrase = $companySymbol.val();
                window.companySymbol = data.phrase;
                return data;
            },
            requestDelay: 400,
            highlightPhrase: true
        };

    $companySymbol.easyAutocomplete(options);
    $('form[name="chart_form"] .easy-autocomplete').parent().find('.invalid-feedback')
        .appendTo($('form[name="chart_form"] .easy-autocomplete'));

    // Client side Validation
    formValidation();

    // Generate Chart
    generateChart();

});

function generateChart() {
    if (!$('#companyOpen').length || !$('#companyClose').length || !$('#companyLabel').length) {
        return
    }
    Chart.defaults.global.pointHitDetectionRadius = 1;
    var openPrice = JSON.parse($('#companyOpen').text());
    var closePrice = JSON.parse($('#companyClose').text());
    const openPrices = [];
    const closePrices = [];

    $.each(openPrice,function (i,v) {
        openPrices.push(parseFloat(v));
    });
    $.each(closePrice,function (i,v) {
        closePrices.push(parseFloat(v));
    });
    var lineChartData = {
        labels: JSON.parse($('#companyLabel').text()),
        datasets: [{
            label: 'Open',
            borderColor: window.chartColors.green,
            pointBackgroundColor: window.chartColors.green,
            fill: false,
            data: openPrices
}, {
        label: 'Close',
            borderColor: window.chartColors.red,
            pointBackgroundColor: window.chartColors.red,
            fill: false,
            data: closePrices
    }]
};

    window.onload = function() {
        var chartEl = document.getElementById('chart');
        window.myLine = new Chart(chartEl, {
            type: 'line',
            data: lineChartData,
            options: {
                title: {
                    display: true,
                    text: 'Open/Close Price By Date For ' + window.companySymbol
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Date'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Values'
                        }
                    }]
                }
            }
        });
    };
}
