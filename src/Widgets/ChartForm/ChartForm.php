<?php
namespace App\Widgets\ChartForm;

use Acme\Validator;
use App\Components\Form\Type\FormTask;
use App\Validator\Constraints\CompanyChecks;
use App\Validator\Constraints\DateChecks;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\NotBlank;

class ChartForm extends FormTask {
    /**
     * @Assert\NotBlank()
     */
    private $companySymbol;

    /**
     * @Assert\NotBlank()
     */
    private $startDate;

    /**
     * @Assert\NotBlank()
     */
    private $endDate;

    /**
     * @Assert\NotBlank()
     */
    private $email;

    public function buildForm(FormBuilderInterface $builder, array $options) {
        // I do not like this, but time is pushing and symfony documentation
        // is, hands down, the worst documentation ever
        // @TODO get the form values through symfony framework, maybe with a magical way and pass them to the constraint
        $startDate = null;
        if (!empty($_POST)) {
            $startDate = $_POST['chart_form']['startDate'];
        }

        $builder
            ->add('companySymbol', TextType::class, [
                'constraints' =>
                    [
                        new NotBlank(),
                        new CompanyChecks()
                    ]
            ])
            ->add('startDate', TextType::class,[
                'constraints' => [
                    new NotBlank(),
                    new Assert\DateTime('Y-m-d')
                ]
            ])
            ->add('endDate', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Assert\DateTime('Y-m-d'),
                    new DateChecks(['custom'=> ['startDate' => $startDate]])
                ]
            ])
            ->add('email', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Assert\Email([
                        'message' => 'The email {{ value }} is not a valid email.',
                    ])
                ]
            ])
            ->add('submit', SubmitType::class);
    }
}
