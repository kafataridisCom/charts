<?php

namespace App\Repository;

use App\Entity\ChartTask;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ChartTask|null find($id, $lockMode = null, $lockVersion = null)
 * @method ChartTask|null findOneBy(array $criteria, array $orderBy = null)
 * @method ChartTask[]    findAll()
 * @method ChartTask[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChartTaskRepository extends ServiceEntityRepository {
    /**
     * ChartTaskRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, ChartTask::class);
    }
}
