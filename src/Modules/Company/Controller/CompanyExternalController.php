<?php

namespace App\Modules\Company\Controller;

use App\Controller\DefaultController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Routing\Annotation\Route;
use App\Helper\CsvHelper;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class CompanyExternalController extends DefaultController {

    public $apiUrl = 'https://www.quandl.com/api/v3/datasets/WIKI/';
    public $apiKey = 'jvuqkX_yS65yY7re-F4S';

    /**
     * @param $companySearchData
     * @return array|bool
     * @throws TransportExceptionInterface
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function retrieveDataFromCompany($companySearchData) {
        $companySearchUrl = $companySearchData->getCompanySymbol()
            .'.csv?order=asc&start_date='.$companySearchData->getStartDate()
            .'&end_date='.$companySearchData->getEndDate()
            .'&api_key='.$this->apiKey;

        $client = HttpClient::create();
        $response = $client->request('GET', $this->apiUrl.$companySearchUrl, ['headers' => [
            'Content-Type' => 'text/csv',
        ]]);

        $statusCode = $response->getStatusCode();
        if ($statusCode == '200') {
            $contents = $response->getContent();
            if ($contents) {
                $companyArray = CsvHelper::toArrayFromString($contents);
                return $companyArray;
            }
        }

        return false;
    }
}
