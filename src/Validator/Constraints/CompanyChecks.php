<?php
namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class CompanyChecks extends Constraint {
    public $message = 'The company symbol "{{ string }}" does not exist';

    /**
     * @return string
     */
    public function validatedBy() {
        return get_class($this).'Validator';
    }
}
