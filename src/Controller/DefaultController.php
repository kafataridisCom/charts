<?php

namespace App\Controller;

use Swift_Mailer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends AbstractController {
    public $theme = "ChartTheme";
    public $data = [];

    /**
     * @param Request $request
     * @param Swift_Mailer $mailer
     */
    public function index(Request $request, Swift_Mailer $mailer) {}

}
