<?php

namespace App\Tests\Modules\Company\Controller;

use App\Entity\ChartTask;
use App\Modules\Company\Controller\CompanyExternalController;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\HttpClient;

class CompanyExternalControllerTest extends TestCase {

    public function testCsvHelperCsVStringToArray() {

        $chartTask = new ChartTask();

        $chartTask->setEmail('test@example.com');
        $chartTask->setCompanySymbol('APPL');
        $chartTask->setEndDate('2020-10-01');
        $chartTask->setStartDate('2020-10-01');

        $companyExternalController = new CompanyExternalController();

        $result = $companyExternalController->retrieveDataFromCompany($chartTask);

        $this->assertEquals (
            false, $result
        );
    }
}
