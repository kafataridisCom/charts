<?php
namespace App\Helper;

class CsvHelper {

    public static function toArrayFromString($string) {
        $lines = explode( PHP_EOL, $string);
        $headers = str_getcsv( array_shift( $lines ) );
        $data = array();
        foreach ($lines as $line) {
            $row = array();
            foreach (str_getcsv($line) as $key => $field) {
                $row[$headers[$key]] = $field;
            }
            $row = array_filter( $row );
            if ($row) {
                $data[] = $row;
            }
        }

        return $data;
    }

}