<?php

namespace App\Entity;

class ChartTask {
    private $companySymbol;

    private $startDate;

    private $endDate;

    private $email;

    /**
     * @return int|null
     */
    public function getId(): ?int {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getCompanySymbol(): ?string {
        return $this->companySymbol;
    }

    /**
     * @param string $companySymbol
     * @return ChartTask
     */
    public function setCompanySymbol(string $companySymbol): self {
        $this->companySymbol = $companySymbol;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getStartDate(): ?string {
        return $this->startDate;
    }

    /**
     * @param string $startDate
     * @return ChartTask
     */
    public function setStartDate(string $startDate): self {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEndDate(): ?string {
        return $this->endDate;
    }

    /**
     * @param string $endDate
     * @return ChartTask
     */
    public function setEndDate(string $endDate): self {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string {
        return $this->email;
    }

    /**
     * @param string $email
     * @return ChartTask
     */
    public function setEmail(string $email): self {
        $this->email = $email;

        return $this;
    }
}
