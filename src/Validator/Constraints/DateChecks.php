<?php
namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class DateChecks extends Constraint {
    public $message = 'End date cannot be older than the start date';
    private $startDate = '';

    public function __construct($options = null) {

        if (($options['custom']['startDate'])) {
            $this->setStartDate($options['custom']['startDate']);
        }

        $options = null;
        parent::__construct($options);
    }

    /**
     * @return string
     */
    public function validatedBy() {
        return get_class($this).'Validator';
    }

    /**
     * @return string|null
     */
    public function getStartDate(): ?string {
        return $this->startDate;
    }

    /**
     * @param string $startDate
     * @return DateChecks
     */
    public function setStartDate(string $startDate): self {
        $this->startDate = $startDate;

        return $this;
    }
}
