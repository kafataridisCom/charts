<?php

namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CompanyRepository")
 */
class Company {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    private $name;
    private $financial_status;
    private $market_category;
    private $round_lot_size;
    private $security_name;
    private $symbol;
    private $test_issue;

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getFinancialStatus(): ?string {
        return $this->financial_status;
    }

    /**
     * @return string|null
     */
    public function getMarketCategory(): ?string {
        return $this->market_category;
    }

    /**
     * @return float|null
     */
    public function getRoundLotSize(): ?float {
        return $this->round_lot_size;
    }

    /**
     * @return string|null
     */
    public function getSecurityName(): ?string {
        return $this->security_name;
    }

    /**
     * @return string|null
     */
    public function getSymbol(): ?string {
        return $this->symbol;
    }

    /**
     * @return string|null
     */
    public function getTestIssue(): ?string {
        return $this->test_issue;
    }

    /**
     * @param string $symbol
     * @return Company
     */
    public function setSymbol(string $symbol): self {
        $this->symbol = $symbol;

        return $this;
    }
}
