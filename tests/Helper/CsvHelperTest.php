<?php

namespace App\Tests\Helper;

use PHPUnit\Framework\TestCase;
use App\Helper\CsvHelper;

class CsvHelperTest extends TestCase {

    public function testCsvHelperCsVStringToArray() {

        $scvString='Date,Open,Ra Ndom
2003-01-02,14.36,Test1
2003-01-03,14.8,Test2';

        $csvArray = CsvHelper::toArrayFromString($scvString);

        $this->assertEquals(
            [
                [
                    "Date"=>"2003-01-02",
                    "Open"=>"14.36",
                    "Ra Ndom"=>"Test1",
                ],
                [
                    "Date"=>"2003-01-03",
                    "Open"=>"14.8",
                    "Ra Ndom"=>"Test2",
                ]
            ],
            $csvArray);
    }
}
