<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class DateChecksValidator extends ConstraintValidator {
    
    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint) {
        if (!$constraint instanceof DateChecks) {
            throw new UnexpectedTypeException($constraint, DateChecks::class);
        }

        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) take care of that
        if (null === $constraint->getStartDate() || '' === $constraint->getStartDate() ||
            null === $value || '' === $value) {
            return;
        }

        if (!is_string($constraint->getStartDate()) || !is_string($value)) {
            // throw this exception if your validator cannot handle the passed type so that it can be marked as invalid
            throw new UnexpectedValueException($value, 'string');
        }

        $start = \DateTime::createFromFormat('Y-m-d', $constraint->getStartDate());
        $end = \DateTime::createFromFormat('Y-m-d', $value);

        if (!$start || !$end) {
            return;
        }

        if ($end->format('U') < $start->format('U')) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', $value)
                ->addViolation();
        }
    }
}
