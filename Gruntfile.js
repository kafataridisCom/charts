module.exports = function (grunt) {

    //the rest should not be changed
    var staticWebURL = 'templates/ChartTheme/assets/public';
    var outDir = 'public/ChartTheme/assets/public';
    var node_modules = 'node_modules';
    var moduleDir = '..';

    var mediaDir = 'templates/ChartTheme/assets/';

    //sass compilation files    TARGET = SOURCE
    //developer can modify as needed
    var sassFiles = {};
    sassFiles[outDir + "/master.css"] = mediaDir + "/css/master.scss";
        // node-sass
    grunt.initConfig({
        sass: {
            options: {
                sourceMap: true,
                implementation: 'sass',
                outputStyle: 'compressed'
            },
            dist: {
                files: sassFiles
            }
        },
        uglify: {
            // options: {
            //     mangle: false
            // },
            plugins: {
                src: [
                    node_modules + "/chart.js/dist/Chart.min.js",
                    node_modules + '/easy-autocomplete/dist/jquery.easy-autocomplete.min.js'
                ],
                dest: outDir + '/plugins.min.js'
            },
            //modify this for any developer js files
            developer: {
                src: [
                    mediaDir + "/js/master.js"
                ],
                dest: outDir + '/developer.min.js'
            },
        },
        cssmin: {
            options: {
                shorthandCompacting: false,
                keepSpecialComments: 0,
                roundingPrecision: -1,
                rebase: true,
                relativeTo: staticWebURL + '/'
            },
            //use this target only for plugins which don't require external resources (such as images/fonts/etc.)
            plugins: {
                src: [
                    mediaDir + "/modules/jquery-ui-1.12.1/jquery-ui.css",
                    node_modules + "/bootstrap-css-only/css/bootstrap.min.css",
                    node_modules + "/easy-autocomplete/dist/easy-autocomplete.min.css",
                    node_modules + "easy-autocomplete/dist/easy-autocomplete.themes.min.css"
                ],
                dest: outDir + '/plugins.min.css'
            },
            //modify this for any developer css files
            developer: {
                src: [
                    outDir + "/master.css",
                    outDir + "/plugins.min.css",
                ],
                dest: outDir + '/developer.min.css'
            },
        },
        concat: {
            options: {
                stripBanners: {
                    block: true,
                    inline: true
                },
                sourceMap: false
            },
            javascript: {
                nonull: true,            //warn if file paths are invalid or missing
                src: [
                    outDir + '/plugins.min.js',
                    outDir + '/developer.min.js'
                ],
                dest: outDir + '/all.min.js'
            }
            , css: {
                src: [
                    outDir + '/plugins.min.css',
                    outDir + '/developer.min.css'
                ],
                dest: outDir + '/all.min.css'
            }
        },
        copy: {
            main: {
                expand: true,
                cwd: 'templates/ChartTheme/assets/modules/jquery-ui-1.12.1/images',
                src: '**',
                dest: 'public/templates/ChartTheme/assets/modules/jquery-ui-1.12.1/images',
            },
        },
    });

    // Plugin loading
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');

    // Task definition
    grunt.registerTask('build', ['sass', 'uglify', 'cssmin', 'concat','copy']);
};
