<?php

namespace App\Tests\Entity;

use PHPUnit\Framework\TestCase;
use App\Entity\Company;

class CompanyTest extends TestCase {

    public function testCompanyGetterSetter() {
        $company = new Company();

        $company->setSymbol('APPL');
        $this->assertEquals('APPL',$company->getSymbol());
    }
}
