<?php

namespace App\Modules\Index\Controller;

use App\Controller\DefaultController;
use App\Entity\Company;
use App\Modules\Company\Model\CompanyModel;
use Symfony\Component\Mime\Email;
use App\Entity\ChartTask;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Modules\Company\Controller\CompanyExternalController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Mailer\MailerInterface;
use Swift_Mailer;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;


class IndexController extends DefaultController {

    /**
     * @Route("/charts", name="charts")
     * @param Request $request
     * @param Swift_Mailer $mailer
     * @return Response
     * @throws TransportExceptionInterface
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function index(Request $request, \Swift_Mailer $mailer) {
        $formResult = $this->handleChartForm($request);

        $this->data['companyData'] = [];
        $this->data['companyDataErrorMessage'] = '';
        if ($formResult) {
            $companyExternal = New CompanyExternalController();
            $this->data['companyData'] = $companyExternal->retrieveDataFromCompany($this->data['formTaskData']);

            if ($this->data['companyData']) {
                $company = (new CompanyModel())->findCompanyBySymbol(
                    $this->getDoctrine()->getRepository(Company::class),
                    $this->data['formTaskData']->getCompanySymbol()
                );

                if (isset($company['name'])) {
                    $result = $this->sendEmail([
                        'subject'=>$company['name'],
                        'setFrom'=>'dev@exapmple.com',
                        'setTo'=>$this->data['formTaskData']->getEmail(),
                        'setBody'=>"From ".$this->data['formTaskData']->getStartDate()." to "
                            .$this->data['formTaskData']->getEndDate()
                    ],$mailer);
                }
            } else {
                $this->data['companyDataErrorMessage'] = 'Unable to load data from endpoint. Please try again later';
            }

        }

        return $this->render($this->theme.'/views/modules/index/index.html.twig', $this->data);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function asyncCheckChartForm (Request $request) {
        $this->handleChartForm($request);
        if ($this->data['formTaskErrors']) {
            $data = ['success' => false, 'errors' => $this->data['formTaskErrors']];
        } else {
            $data = ['success' => true];
        }

        return new Response(json_encode($data));
    }

    /**
     * @param Request $request
     * @return bool
     */
    private function handleChartForm(Request $request) {
        $chartTask = new ChartTask();
        $chartsForm = $this->createForm('App\Widgets\ChartForm\ChartForm', $chartTask);

        $this->data['formTaskData'] = [];
        $this->data['formTaskErrors'] = [];
        $chartsForm->handleRequest($request);
        if ($chartsForm->isSubmitted() && $chartsForm->isValid()) {
            $this->data['formTaskData'] = $chartsForm->getData();
            $result = true;
        } else {
            $this->data['formTaskErrors'] = $this->getErrorsFromForm($chartsForm);
            $result = false;
        }

        $this->data['chartsForm'] = $chartsForm->createView();

        return $result;
    }

    /**
     * @param FormInterface $form
     * @return array
     */
    private function getErrorsFromForm(FormInterface $form) {
        $errors = array();
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }
        return $errors;
    }

    /**
     * @param $options
     * @param Swift_Mailer $mailer
     * @return bool|int
     */
    public function sendEmail($options, \Swift_Mailer $mailer) {
        if (!isset($options['subject'])) {
            return false;
        }

        if (!isset($options['setFrom'])) {
            return false;
        }

        if (!isset($options['setTo'])) {
            return false;
        }

        if (!isset($options['setBody'])) {
            return false;
        }

        $message = (new \Swift_Message($options['subject']))
            ->setFrom($options['setFrom'])
            ->setTo($options['setTo'])
            ->setBody($options['setBody']);

        return $mailer->send($message);;
    }
}
