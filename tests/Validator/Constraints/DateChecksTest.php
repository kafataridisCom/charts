<?php

namespace App\Tests\Validator\Constraints;

use App\Validator\Constraints\DateChecks;
use PHPUnit\Framework\TestCase;

/**
 * @Annotation
 */
class DateChecksTest extends TestCase {

    public function testValidatedBySuccessWithOptions() {
        $dateChecks = new DateChecks(['custom'=>['startDate'=>'2020-10-01']]);
        $validatorClassName = $dateChecks->validatedBy(get_class($dateChecks).'Validator');
        $this->assertEquals ('App\Validator\Constraints\DateChecksValidator', $validatorClassName);
    }

    public function testValidatedByFailureWithNoOptions() {
        $dateChecks = new DateChecks();
        $validatorClassName = $dateChecks->validatedBy(get_class($dateChecks).'Validator');
        $this->assertEquals ('App\Validator\Constraints\DateChecksValidator', $validatorClassName);
    }
}