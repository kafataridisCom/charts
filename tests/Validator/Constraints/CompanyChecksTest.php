<?php

namespace App\Tests\Validator\Constraints;

use App\Validator\Constraints\CompanyChecks;
use PHPUnit\Framework\TestCase;

/**
 * @Annotation
 */
class CompanyChecksTest extends TestCase {

    public function testValidatedBy() {
        $companyChecks = new CompanyChecks();
        $validatorClassName = $companyChecks->validatedBy(get_class($companyChecks).'Validator');
        $this->assertEquals ('App\Validator\Constraints\CompanyChecksValidator', $validatorClassName);
    }
}
