###Prerequisities 
* PHP 7.2+
* Mysql or MariaDB
* NPM (latest)
* Composer (latest)
* Symfony cli

####Create a new directory to host the app
```bash
$ cd ~ 
$ mkdir mxCharts
$ cd mxCharts
```

#####Checkout from public repo `https://bitbucket.org/kafataridisCom/charts/src/master/`
```bash
$ git clone https://kafataridisCom@bitbucket.org/kafataridisCom/charts.git .
```

####Dependency Installation
These will install node_modules dir, and grunt will build the various css/js files into their 
minified/concatenated versions
```bash
$ npm install 
$ grunt build
```

Install symfony dependencies
```bash
$ composer install
```


#####Create a database and update your DATABASE_URL like the one below 
```
DATABASE_URL=mysql://xmcharts_user:k6BLYLHwMuTZhocK@127.0.0.1:3306/xmcharts
```
Import the file "db_files/xmcharts.sql" (Nasdaq company list) or execute it with cli doctrine

Finally to view the app
```$xslt
$ cd ~/mxCharts
$ symfony server:start
```

###Extras
In order to verify the mail send functionality
1. Create an account in mailtrap.io (all emails will be hijacked and they will be visible there)
1. Select the "Symfony4+" configuration from the dropdown in your account settings  
1. Copy it the configuration string
1. Paste the configuration string in the MAILER_URL in ./.env file
``` 
MAILER_URL=smtp://smtp.mailtrap.io:2525?encryption=tls&auth_mode=login&username=1ff86a28515806&password=78c9aff07d5f9e
```

###Various hiccups
* Symfony has one of the worst documentations I have ever seen in my life. Also it is my first time building anything 
with this framework.
* Quandl threw a lot of 403, 429 errors etc. and by the time I started working on the api, someone else had already 
maxed out their free requests. Created an account to continue but their service continued to be unavailable for me.
Hopefully I had retained some offline data and I tried to work with that.
* If the api url continues to not serve anything please replace the url here 
```
https://bitbucket.org/kafataridisCom/charts/src/41babf594bdf4032ddc36e8174717290544730a9/src/Modules/Company/Controller/CompanyExternalController.php#lines-34
```
with
```
https://www.kx-plastics.gr/WIKI-AAPL.csv
```
So the final result would be this
```php
        $response = $client->request('GET', "https://www.kx-plastics.gr/WIKI-AAPL.csv", ['headers' => [
            'Content-Type' => 'text/csv',
        ]]);
```

Not the best solution, but I had no access to Quandl data at the time

######Further instructions or screenshots can be provided upon request
