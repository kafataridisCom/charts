<?php

namespace App\Modules\Company\Controller;

use App\Controller\DefaultController;
use App\Modules\Company\Model\CompanyModel;
use App\Entity\Company;
use App\Repository\Company\CompanyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CompanyController extends DefaultController {

    /**
     * @param Request $request
     * @return Response
     */
    public function searchCompanyFromSymbolOrName(Request $request): Response {

        $requestString = $request->get('phrase');

        $entities = (new CompanyModel())->searchCompanyFromSymbolOrName(
            $this->getDoctrine()->getRepository(Company::class),
            $requestString
        );

        if(!$entities) {
            $entities = self::modifyEntities($entities);
        }

        return new Response(json_encode($entities));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getCompanyFromSymbol(Request $request): Response {

        $requestString = $request->get('symbol');

        $company = (new CompanyModel())->findCompanyBySymbol(
            $this->getDoctrine()->getRepository(Company::class),
            $requestString
        );

        return new Response(json_encode($company));
    }

    /**
     * @param $entities
     * @return mixed
     */
    public static function modifyEntities($entities){

        $modifiedEntities = [];
        foreach ($entities as $index => $entity){
            $modifiedEntities[$entity['symbol']] = $entity['name'].' ['.$entity['symbol'].']';
        }

        return $modifiedEntities;
    }
}