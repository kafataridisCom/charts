<?php

namespace App\Modules\Company\Model;

use App\Entity\Company;
use App\Repository\Company\CompanyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CompanyModel {

    /**
     * @param $object
     * @param $requestString
     * @return Response
     */
    public function searchCompanyFromSymbolOrName($object, $requestString) {
        return $object->searchEntitiesByString($requestString, $requestString);
    }

    /**
     * @param $object
     * @param $requestString
     * @return mixed
     */
    public function findCompanyBySymbol($object, $requestString) {
        return $object->findCompanyBySymbol($requestString);
    }

}