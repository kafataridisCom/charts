<?php

namespace App\Tests\Modules\Company\Controller;

use App\Modules\Company\Controller\CompanyController;
use PHPUnit\Framework\TestCase;

class CompanyControllerTest extends TestCase {

    public function testCsvHelperCsVStringToArray() {

        $modifiedEntity = CompanyController::modifyEntities([
            [
                'symbol'=>'APPL',
                'name'=>'Apple Inc.'
            ],
            [
                'symbol'=>'AEY',
                'name'=>'AEY Inc.'
            ],
        ]);

        $this->assertEquals (
            [
                "APPL"=>'Apple Inc. [APPL]',
                "AEY"=>'AEY Inc. [AEY]'
            ],
            $modifiedEntity
        );
    }
}
